<?php


namespace Kowal\MagentoWidgetHostingKowal\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Exception\LocalizedException;

class MagentoHosting extends Template implements BlockInterface
{

    protected $_template = "widget/magentohosting.phtml";


    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        Context $context,
        array $data = [],
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Framework\Pricing\Helper\Data $dataHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->dataHelper = $dataHelper;
        $this->imageHelper = $imageHelper;
        $this->productRepository = $productRepository;

        parent::__construct($context, $data);


    }

    public function getProductsCollection($skus)
    {

        $aSkus = explode(",", $skus);


        $collection = $this->collectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('sku', array('in' => $aSkus));
        $collection->setOrder('price', 'ASC');
        $collection->setPageSize(3)->load();
        if ($collection->count() >= 3) {

            $i = 1;
            $p = [];
            foreach ($collection as $key => $product) {
                $p[$i] = $product;
                $i++;
            }

        } else {
            throw new LocalizedException(__('Proszę podać 4 istniejące SKU!'));
        }
        return $p;
    }


    public function getDropdownText($_product, $attr_code, $optionId)
    {
        $attr = $_product->getResource()->getAttribute($attr_code);
        if ($attr->usesSource()) {
            return $attr->getSource()->getOptionText($optionId);
        } else {
            return false;
        }
    }


    public function getProductPrice($product)
    {

        $price = $product->getPriceInfo()->getPrice('final_price')->getValue();
        $formattedPrice = $this->dataHelper->currency($price, true, false);

        return $formattedPrice;
    }

    public function getImageUrl($_product)
    {
        return $this->imageHelper->init($_product, 'product_base_image')->getUrl();
    }

    public function getProductUrl($product)
    {
        $product_ = $this->productRepository->getById($product->getEntityId());
        return $product_->getUrlModel()->getUrl($product_);
    }
}

